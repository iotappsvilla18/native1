import React from 'react';
import { StyleSheet, Text, View, ScrollView, ActivityIndicator, Image } from 'react-native';
import Nav from './src/Nav/Nav';
import Generator from './src/Generators/Generators';
import ListItem from './src/Generators/ListItem';
import Input from './src/Input/Input';
import PickerComponent from './src/Picker/Picker';
import Image1 from './Assets/Images/beautiful-place.jpg';
import ModalComponent from './src/Modal/Modal';


export default class App extends React.Component {

  state = {
    name: 'My First App',
    random: [20,13],
    loading: false
  }

  onAddRandom = () => {
    const random = Math.floor(Math.random()*100) + 1;
    this.setState(prevState => {
      return {
        
        random: [...prevState.random, random]
        // places: prevState.places.concat({
        //   key: Math.random(),
        //   value: placeName
      }
    })
  }

  onItemDelete = (i) => {
    const newArray = this.state.random.filter((item, index)=>{
        return i !== index;
    });
    this.setState({
      random: newArray
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Nav name={this.state.name}/>
        <ScrollView 
          style={{width:"100%"}}>
        <View style={styles.wrapper}>
          <Generator add={this.onAddRandom}/>
          <ListItem 
            items={this.state.random}
            delete={this.onItemDelete}/>
          {/* <Input /> */}
          {/* <PickerComponent />
          <ActivityIndicator 
            size="large"
            color="salmon"
            animating={this.state.loading}/> */}
            {/* <Image 
              source={{uri: 'https://picsum.photos/200/300'}}
              style={styles.image} /> */}
            <ModalComponent />
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 20,
  },
  View2: {
    width: '100%',
    backgroundColor: 'blue',
  },
  wrapper: {
    flex: 1,
    width: '100%',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  image: {
    width: '100%',
    height: 200,
    marginTop: 20,
  }
});
