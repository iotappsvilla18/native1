import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

class Input extends Component {
    state = {
        myInput: "",
        users: ['John', 'James', 'Kedar', 'Vishal', 'Vipul', 'Aamanya']
    }

    onChangeHandler = (event) => {
        this.setState({
            myInput: event
        })
    }

    onAddUser = () => {
        this.setState(prevState => {
            return {
                myInput: '',
                users: [...prevState.users, prevState.myInput]
            }
        })
    }
    
    render () {
        return (
           
            <View style={styles.view}>
                <TextInput 
                    value={this.state.myInput}
                    style={styles.input}
                    onChangeText={this.onChangeHandler}/>

                <Button 
                    title="Add User"
                    onPress={this.onAddUser}/>
                {
                    this.state.users.map(item => (
                        <Text style={styles.list} key={item}>{item}</Text>
                    ))
                }
            </View>
           
        );
    }
};

const styles = StyleSheet.create ({
    input: {
        width: "100%",
        backgroundColor: '#ececec',
        marginTop: 20,
        fontSize: 20,
        padding: 10,
    },
    view: {
        width: '100%'
    },
    list: {
        width: '100%',
        backgroundColor: '#f2f2f2',
        alignItems: 'center',
        fontSize: 40,
        borderWidth: 2,
        marginBottom: 20,
    }
})

export default Input;