import React, {Component} from 'react';
import { StyleSheet, Text, View, Modal, Button } from 'react-native';

class ModalComponent extends Component {
    
    state = {
        modal: false
    }
    
    modelHandler = () => {
        this.setState({
            modal: !this.state.modal ? true : false
        })
    }

    render () {
        return (
            <View style={styles.modal}>
                <Button 
                    title="Open"
                    onPress={this.modelHandler}/>
                <Modal 
                    visible={this.state.modal}
                    animationType={'fade'}
                    onShow={()=>{
                        alert('visible')
                    }}>
                    <View style={{marginTop: 20, backgroundColor: 'salmon' }}>
                        <Text>My Modal Component</Text>
                    </View>
                    <Button 
                        title="Close"
                        onPress={this.modelHandler}/>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        width: '100%',
    }
})

export default ModalComponent;