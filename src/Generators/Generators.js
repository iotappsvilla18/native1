import React from 'react';
import { StyleSheet, Text, View, Button, TouchableWithoutFeedback } from 'react-native';

const Generator = (props) => (
    <TouchableWithoutFeedback 
        onPress={props.add}>
    <View style={styles.generate}>
        <Text>Add Number</Text>
    </View>
    </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
    generate: {
      padding: 10,
      alignItems: 'center',
      backgroundColor: '#00bcd4',
      width: '100%',
      marginTop: 20,
    }
  });

export default Generator;