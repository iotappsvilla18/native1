import React, {Component} from 'react';
import { StyleSheet, Text, View, Picker, Slider } from 'react-native';

class PickerComponent extends Component {
    
    state ={
        language: "vishal",
        value:50
    }

    sliderChangeHandler = (value) => {
        alert(value)
    }

    render () {
        return (
            <View style={{width: '100%'}}>
            
            <Picker
                style={{width: '100%'}}
                selectedValue={this.state.language}
                onValueChange={(itemValue, itemIndex)=>{
                    this.setState({language: itemValue})
                }}>
                <Picker.Item 
                    label="Kedar"
                    value="kedar"/>
                <Picker.Item 
                    label="Vishal"
                    value="vishal"/>
            </Picker>
            <Slider 
                value={this.state.value}
                maximumValue={100}
                minimumValue={10}
                step={10}
                onValueChange={this.sliderChangeHandler}/>
            </View>
        );
    }
}

export default PickerComponent;